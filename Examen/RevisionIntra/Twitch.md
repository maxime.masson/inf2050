# Revision examen Intra (Twitch)

## Repository 

En français, le `repository` est un **dépôt**.

<br>

## Dernier commit

Pour modifier le message du dernier commit, il suffit de faire la fonction suivante : 

```git
$ git commit --amend -m "<nouveau message>"

```

<br>

## Outils d'analyse static

**Exemple : Checkstyle**

Outil qui annonce les erreurs de mise en forme. La plus part du temps l'outil analyse la **volumétrie**, la **complexité cyclomatique**.

Ces statistiques sont utiles pour porter un jugement sur le code source complet.

Pour modifier automatiquement la mise en forme, il existe d'autre outil qu'on nomme `Beautifier`.

<br>

## Logiciel libre

Il y aura des questions pointus sur la compréhension des différentes `Licenses` .
Un résumé `Wiki` serait amplement suffisant.

    - MIT 
    - GPLv3
    - CC-by-sa (Stackoverflow)
        - Finalement le code n'est pas sous **MIT**. 
        - License : Credit common (Share-Alike), similaire a GPLv3.

<br>
Un logiciel libre est un logiciel qui fait respecté les 4 droits fondamentaux

    - Ce que je veux
    - Modifier
    - Distribuer sans modif
    - Distribuer avec modif

Ces droits sont tous sous le principe d'une license libre.

<br>

## GPLv3 et Creative Commun (Share-alike)

Copy-left : License qui est virale. 
`GPLv3` : Est une license virale. Le code source OÙ celui sous la license sera utilisé doit AUSSI être sous license `GPLv3`.

CC-BY-SA

<br>

## Convention de dommage

Comment nommer/écrire les variables. 

    - Pascal case (AlloTest)
    - camelCase (alloTest)
    - Snake case (allo_test)
    - Upper snake case (ALLO_TEST)
    - List case / Kebab case (allo-test)

<br>

## Cycle de vie d'un projet Maven

Le lifecycle de maven est la séquence de toute les cibles. 
En Ant, le lifecyle est la séquence de phase.

Les phases ne font rien, ce sont des `place-holder` où nous allons mettre du code.

Goal - Créer des répertoires, compiler du code, etc.
Plugins - Packet de classe Java (une classe est un `Goal`). Permet d'attacher des goals ensemble.

### **Exemple**

Dans IntelliJ, un build passe au travers de toute la chaine s'il est configué pour travailler avec Maven.

### Dépendance Maven

Ce qui permet d'identifer une dépendance

    - GroupeID
    - ArtifactID
    - Num de version

<br>

## Pyramide des tests

Bas : Les test le plus rapide, le plus facile à maintenir, les plus nombreux. `Test unitaire`.

Haut : Les tests fonctionnels/UI, les plus lents et les plus fragiles, les moins nombreux `Test UI`.

Milieu : Les tests d'intégrations.

<br>

## Annotation JUNIT

Il existe une annontation en Java.

### @Test
### @BeforeEach

Fonction appelée avant chaque test unitaire.

### @AfterEach

Fonction appelée après chaque test.

### @BeforeAll

Fonction exécutée une seule fois, au début de TOUS les tests.

<br>

## Loi d'Emether

Règle qui définie les droits concernant les paramètres, les classes.

Essentiellement, ce que nous pouvons utiliser : 

    this
    les objets reçu en paramètre
    variable dinstance qui découle de this

J'ai le droit d'appeler des méthodes sur les objets en paramètre, sur l'objet `this`. Doit d'instancier des objets et d'appeler des méthodes sur ces objets la.

<br>

## La dette technique

Je livre des fonctionnalités maintenant même si je n'ai pas tout compris le problème.

J'ai la capacité de refactoriser mon code lorsque je comprendrais mieux le problème.

En bref : `Livrer maintenant, ajuster plus tard`.

<br>

## Licenses CC0 et WTFPL

Sont des licenses qui `simule` du domaine public.

Donne la permission à d'autre personne de l'utiliser etc. mais reste une liencese.