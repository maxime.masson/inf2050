# Test J UNIT 5

## Test d'intégration

Assure que les interactions entre les modules fonctionnent correctement. Le terme est souvent réservé à l'utilisation de réels services externes (ex : une base de données, un service de courriel, un module de paiement, etc.).

- Tests sont plus lent mais offrent une `meilleure idée du bon fonctionnement du système`.

## Tests de contrats

**Un contrat** est la spécification d'un module fournisseur - étant ces `entrées`, le fournisseur s'engage à produire ces sorties.

- Tests de contrat assurent que la spécification est respectée.

## Tests de bout en bout

Tout ce que je voulais est intégré, d'où `bout en bout`.
- Très fragile et couteux

## Tests d'acceptation

Qu'est-ce que ça prend pour dire ` C'est bon `.
Parfois automatisé, parfois non.


# Recette

## Comment écrire un test unitaire ?

- Chaque classe applicative doit avoir une classe de test
- Seule l'interface publique d'une classe doit être testée
- Les méthodes triviales (ex get, set) n'ont **pas** à être testées
- Chaque test devrait être indépendant des autres et répétable
- S'en tenir à la structure `Préparation, Action, Affirmation`.


# Ecrire un test

## Assert

- assertTrue(condition, message?);
- assertFalse(condition, message?);
- assert[Not]Equals(expected, actual, message?);
- assert[Not]Same(expected, actual, message?);
- assert[Not]Null(expected, actual, message?);

```
@Test
void additionne() {
    final var un = new Monnaie("1");
    final var deux = new Monnaie("2");

    final var resultat = plus(un,un);

    assertEquals(deux, resultat, "1$ + 1$ = 2$");
}
```

```
@Test
void multiplie() {
    final var deux = new Monnaie("2");
    final var trois = new Monnaie("3");
    final var six = new Monnaie("6");

    final var resultat = mult(deux,trois);

    assertEquals(six, resultat, "2$ x 3$ = 6$");
}
```

## beforeEach()

Cette méthode est appelé avant `chaque method test`. Très pratique pour mettre en place la situation initiale.

## beforeAll()

Doit être statique.  Elle va être exécuté seulement `1 fois` avant le **tout premier test**.

## AfterEach()

Code pour nettoyer dans le cas que les tests ajoute ou modifie le programme.

## AfterAll()

Code pour nettoyer dans le cas que les tests ajoute ou modifie le programme.

Exemple de début de test
1- `beforeAll()` est lancé
2- Premier test, `beforeEach()` est lancé.

## @DisplayName

Permet de donner un nom au test

## Disabled

Sert à désactiver un test.
La notation est utilisé afin de `ne pas mettre de test en **commentaire**`.
```
Structure: @Disabled("raison en commentaire");
```

## assertThrows

Sert à tester plusieurs Exceptions dans un même test.

## assertAll

Sert à regrouper plusieurs test ensemble

Exemple
```
assertAll("nombres entiers",
() -> assertNotNull(new Monnaie("1");
() -> assertNotNull(new Monnaie("10");
() -> assertNotNull(new Monnaie("100");
)
```
## assertTimeout(ofSeconds(x), function() {})

Sert à tester la durée d'une fonction. Le test échoue si la fonction prend trop de temps à compléter.

Très utile lors d'attente de niveau de service, communication avec des bases de données, etc.

## assertTimeoutPreemptively(ofSeconds(x), function() {}) 

Très similaire à son cousin assertTimeout mais `force` le shutdown de la méthode testée.

## private static Stream<Arguments> function() {}

Similaire à une boucle, mais plus performant car permet de pointer quel test est brisé.

```
// Exemple avec le langage Mini Scheme (Françcois, INF2050)
private static Stream<Arguments> additionneMonnaieCas() {
    return Stream.of(
        Arguments.of(new Monnaie("1"), new Monnaie("1"), new Monnaie("2")),
        Arguments.of(new Monnaie("1"), new Monnaie("2"), new Monnaie("3")),
        Arguments.of(new Monnaie("0"), new Monnaie("0"), new Monnaie("0"))
    );
}
```