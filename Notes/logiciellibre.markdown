# Logiciel Libre

## Qu'est-ce qu'un Logiciel Libre ?

C'est un Logiciel sans propriétaire. Un logiciel qui respecte les droits fondamentaux des utilisateurs, c'est-à-dire, d'exécuter, de copier, de distribuer...

### Droit d'auteur

Le droit d'auteur est le `droit exclusif de produire, de reproduire, de publier ou d'exécuter` une oeuvre originale.
Pour céder un droit d'auteur, il doit être écrit `explicitement` dans le contrat.

### Brevet

Les brevets `accordent un droit exclusif limité dans le temps et protégé légalement pour créer, utiliser et vendre` une invention.

### Marque de commerce

Combinaison de lettres, de mots, de sons ou de symboles qui différencie les produits, services d'une compagnie.

### Licence

Autorisation donnée à un `licencié` par un concédant lui permettant d'utiliser sa propriété intellectuelle.

Elle décrit `les droits et les obligations` des deux parties ainsi que les limitations.

### Logiciels Propriétaires

Logiciel entièrement protégé par les lois sur le droit d'auteur.

<br>

# Licence de logiciel libre

Une `licence de logiciel libre` ne cède **pas** les droits d'auteur des programmeurs originaux. 

Elle donne plutôt l'autorisation à d'autres programmeurs d'utiliser le programme SI un `ensemble de conditions est respectées`.

```
Dès que ce n'est plus le cas, les permissions sont retirées et le licencié n'a plus le droit d'utiliser la propriété intellectuelle.
```

<br>

# Permissions Accordées

- Le licencié peut utiliser le logiciel à des fins commerciales ou privées
- Le licencié peut modifier le logiciel et redistribuer le logiciel

## Conditions

- Si le logiciel est redistribué, le licencié doit `rendre le code source disponible`
- Si le logiciel est redistribué, une copie du texte de la lience et/ou les notices de droits d'auteur `doivent être incluses`.
- L'utilisation du logiciel via le réseau `est une forme de distribution`.
    - MangoDB en est un exemple.
- Si le code source est **modifié**, les modifications doivent être **redistribuées** avec la même licence _(share alike)_.
- Si le code source est modifié, les modifications doivent être clairement indiquées _(attribution)_.

- Les auteurs et les contributeurs du logiciel ne peuvent pas être tenus responsable des dommages causés par le logiciel
- Le logiciel distribué sans aucune garantie. Tous les risques liés à l'utilisation du logiciel sont assumé par le licencié

<br>

# Licences Libres

## MIT

Donne toutes les permissions et ne requiert que `les notices de copyright et le texte de la lience` dans le code. N'offre aucune autre protection.

Vue sur Github.

## BDS (3-clauses)

Similaire à la licence `MIT` mais ajoute une restriction par rapport à l'utilisation du nom des détenteurs des droits d'auteur et des contributeurs à des fins promotionnelles

Exemple : Si je modifie un code MySQL, je ne peux plus l'appeler MySQL.

## APACHE 2.0

Donnent `toutes les permissions` en plus d'octroyer **explicitement** une licence sur les brevets. 

Requiert
- Notices de copyright
- Texte de la licence
- Modifications apportés clairement mentionnées.

N'offre aucune protection sur le `maintient des permissions accordées`.

## GPLv3

Un programme qui utilise une librairie GPL doit `obligatoirement` être sous Licence GPL. 

Il est donc impossible de prendre du code `GPL`, de cacher l'implémentation et de le revendre autrement.

## LGPLv3

Sous LGPL, l'utilisation d'une librairie GPL ne force pas de licencé sous GPL.
Façon de permettre aux autres d'utiliser la librairie sans être sous GPL.

## AGPLv3

GPL3 + 1 condition supplémentaire
- Si ton logiciel est accessible par le `réseau`, il est une forme de `distribution`.

N'est pas une forme de combinaison.

Est une forme de distribution

<br>

# Je lègue mes droits d'auteur

Certaines licences sont utilisées afin de lever les droits d'auteur. Elles tentent en autres mots de `mettre le programme dans le domaine public` 
- CC0
- Unlicense
- WTFPL

<br>

# Ai-je le droit d'utiliser le code ?

- Si le logicel a été distribué `sans license d'utilisation` ou `sans autorisation` et qu'il n'est pas dans le domaine public, alors non;

- Si, à n'importe quel moment, les conditions de la licence ne sont pas respectés, alors non;

- Si les algorithmes du logiciel sont brevetés et que la licence n'octroie pas `explicitement` une license de brevets, c'est **probablement** permis.

<br>

# Quoi utiliser ?

- Le logiciel est-il un produit complet ou une librairie ?
    - Pour une librairie : Préférer les licences MIT, Apache 2.0 ou LGPLv3

- Est-ce un problème si une compagnie utilise le logiciel dans des produits propriétaire ?
    - Considérer une licence `copyleft` comme GPLv3 ou AGPLv3

- Est-ce important de distinguer le code source original des modifications apportées par des contributeurs ?
    - Considérer Apache 2.0 ou GPLv3

- Est-ce important de protéger les contributeurs et les utilisateurs contre les pourduites liées à l'utilisation d'algo breveté ?
    - Apache 2.0 ou GPLv3

- Le logiciel combine t'il d'autres logiciels sous licenses libres ?
    - Vérifier la **compabilité entre les licenses** et les restrictions

- Est-ce important que tous puissent l'utiliser comme bon leur semble ?
    - CC0 ou WTFPL

<br>

# Modèles d'affaires d'un logiciel libre

- Vente de support, de services conseils et de livres
- Services de formations et de certifications
- Vente de fonctionnalités additionnelles payantes
- Revenus publicitaires (Mozilla offre)
- Développements de fonctionnalités sur mesures
- Services en ligne (SaaS)
- Développement coopératif

<br>

# Opportunités vs Risques (Logiciel Libre)

## Opportunités

## Risques

```
with commercial software, the customer PAYS THE VENDOR to manage the risks.

in the world of open source, YOU must manage the risks
```

<br>

# Choisir un Logiciel Libre pour son entreprise

Il existe plusieurs opportunités et plusieurs risque.

## Opportunités

- Contrairement à une solution `propriétaire`, il est possible de se faire une idée de la qualité du produit et même de l'essayer avant son acquisition

- Il est difficile de cacher les problèmes et les lacunes du logiciel

- Il est possible d'adapter le logiciel aux besoins de la compagnie `sans attendre le commanditaires ou les volontaires`.

- Risque de perte de contrôle très minime.

- Dans le cas de la faillite du commanditaire, le logiciel est toujours disponible

## Risques

- Il n'y a aucune personne contact et/ou ressource.

- Pour obtenir de l'information, il faut chercher soit même dans les FAQ, les forums, les billets, etc.

- Si le code source est modifié, la compagnie devient responsable de l'application et des mises à jour.

<br>

# Ensemble minimal d'outils

Il faut un `Site Web`, un canal de communication principal pour le grand public et les futurs participants.

Intéressant d'avoir un `système de gestion de source`.

Discussion en temps réel (Slack, RocketChat, etc.).

## Les outils extra

Il est intéressant d'avoir des outils extra afin d'ajouter une touche supérieure. 
- Un wiki pour regrouper la documentation du projet
- Un forum de question-réponse
- Une présence sur les réseaux sociaux afin d'annoncer des événements divers ou reliés au projet
