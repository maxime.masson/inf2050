# Notes du cours 2050


## Commandes Git 

`git init` : Permet d'initialiser le repo de git

`git status` : Permet de voir l'état du repository du contrôle de version Git

`git add` : Permet d'ajouter la modification du fichier dans l'espace de préparation, appelé **Staging Area**

`git rm --cached` : Permet d'enlever le **tracking** du fichier de l'espace de préparation, appelé **Staging Area**

`git commit` : Permet de créer un **commit** avec les changements courants

`git log` : Liste tous les commits en ordre chronologique inverse

`git rm` : Supprime le fichier et les changements du staging area.

`git mv` : Déplace un fichier du Staging Area et ajoute l'opération à l'index

`git diff` : Montre les différences entre le staging area et le local directory
- `git diff HEAD` : Affiche les différences entre le working tree et le commit à HEAD
- `git diff <commit>..<commit>` : Affiche les différences entre deux commits

`git reset` : 
- `git reset --hard HEAD` : Remet l'index et le working directory à leur valeur au moment du commit HEAD

`git clean -df` : Supprime les fichiers et les répertoires qui ne sont pas connus de l'index.

`git stash` : Permet de mettre de côté les modifications faites au working tree et à l'index
- `git stash apply` : Remet les modifications dans le working tree

`git revert <commit>` : Crée un nouveau commit qui renverse les opérations du commit spécifié en argument.

`git fecth <remote>` : Télécharge le CONTENU dans le remote nommé.

`git merge --abord`: Annule la tentative de fusion

<br>

## Les commits

Plusieurs informations importantes sont liées aux commits. 
1. L'identifiant unique *(SHA)*
2. L'auteur avec adresse courriel
3. L'heure
4. Le message du commit 

`git commit -m` : Permet de lier un message descriptif au commit.

`git commit --amend` : Permet d'aller modifier le dernier commit.


## Le répertoire .git

On peut accéder à tous les objets de git dans le réperture `.git/Objects`. Ils sont répertoriés avec leur identifiant unique. 
Tout est conservé *localement*.

## Le fichier .gitignore 

Ce fichier sert à lister `TOUS LES FICHIERS` qui doive être ignoré. Git fera donc *abstraction* des fichiers compris dans la liste. 
`.gitignore` fonctionne comme un fichier texte.

Chaque fichier doit être listé ligne par ligne.
**Le fichier prend en compte les RegEx GLOB**.

## Mettre à jour le repo Git local par rapport au repo Git upstream (Github/Gitlab...)

Il suffit de `merger` le repo du repo en ligne.  
```git
git fetch upstream
git checkout master
git merge upstream/master
```

# La gestion de source

## Quoi Inclure ?

On met sous gestion de source tout le `code source` écrit.
- Les tests
- La documentation interne
- Les scripts de build
- Les manifestes de dépendances
- Les fichiers de configuration
- Les fichiers binaires nécessaires au déploiement
    - Inclus parfois les librairies dépendantes
    - Certains utilitaires de build

## Quoi Exclure ?

On exclut tout ce qui `se génère` à partir du code source. 
- Le code compilé
- La documentation formatée
- Les pages de résultats des tests
- Tout ce qui peut être téléchargé de façon répétable ou automatique
- Les configurations personnelles

# Fonctionnement interne en bref

## Working Tree

C'est le répertoire de travail habituel. Ce sont les fichiers et répertoires qu'y sont créés, supprimés et modifiés avec un editeur de sources.

Souvent appelé `Working Directory`.

## Index

C'est l'espace intermédiaire ou on **prépare** le prochain `commit`.
Souvent appelé `Staging Area` ou `cache`. 

## Conflits

Le processus de résolution de conflits est habituellement simple et direct.

Les conflits surviennent `lorsqu'une même ligne de code a été modifiée par un autre programmeur`.
- git présente les deux versions et demande à choisir

### Exemple de conflits

```git 
Première ligne de texte.
<<<<<<< HEAD
Modification faite par moi.
=======
Modification faite par un autre programmeur, récupérée lors du "pull".
>>>>>>> 453ee48dec6929be4b799be79f9d772897a431c7
Ancienne seconde ligne de texte.
```

### Chercher si le délimiteur de conflits est présent ou non.

Il suffit de lancer cette commande : 
```bash
$ find . -path .git -prune -o -type f | xargs grep '===='
```

# Collaboration

Un `repository`n'est qu'un répertoire contenant des fichiers. Le but est de rendre possible la copy du repository local vers une machine distante.

En git, le serveur distant est nommé **REMOTE** et sa copie du repository est nommé **REMOTE REPOSITORY**.

## Identifiant unique

Git assigne un identifiant unique à tous les objets du repository. Il est calculé à `partir du contenu des objets`.

--> Algorithme de hachage `SHA-1` pour générer l'identifiant (hash).
- de cette façon, l'unicité est assuré sans serveur central de synchronisation.

DE PLUS, le `hash` d'un `commit` est calculé à partir du hash du commit **précédent** ET du hash du répertoire à la racine du projet.
- Structure similaire à celle d'un `blockchain`.
- Assure l'intégrité de l'historique du projet.

## SSH & CLÉS

- Un `remote repository` est un repo git sur une autre machine
- L'outil habituel pour se connecter à une machine distante est SSH. Git utilisent SSH.
- Le mode d'identification au remote peut être fait par username/password ou par clé SSH (`clé publique/clé privée`).

** Il existe de nombreux tutoriels sur la création d'une paire de clés SSH


