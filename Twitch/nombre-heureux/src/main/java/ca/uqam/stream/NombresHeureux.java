// package ca.uqam.stream;
// import java.util.*;

// public class NombresHeureux {

//   public static boolean estHeureux(final int n) {
//     return do_estHeureux(n, new LinkedList<Integer>());
//   }

//   private static boolean do_estHeureux(final int n, List<Integer> vus) {
//     if (n == 1) {
//       return true;
//     } else if (vus.contains(n)) {
//       return false;
//     } else {
//       vus.add(n);
//       return do_estHeureux(sommeCarresChiffres(n), vus);
//     }
//   }

//   private static int sommeCarresChiffres(final int n) {
//     return Arrays.stream(String.valueOf(n).split(""))
//       .mapToInt(Integer::valueOf)
//       .map(n -> n * n)
//       .sum()
//       ;
//   }
// }